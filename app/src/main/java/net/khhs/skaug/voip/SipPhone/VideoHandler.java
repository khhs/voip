package net.khhs.skaug.voip.SipPhone;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.Uri;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class VideoHandler{

    MediaRecorder CamRec;

    private LocalSocket receiver;
    LocalSocket sender;

    public VideoHandler(SurfaceView sView){
        sView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                CamRec = new MediaRecorder();
                CamRec.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
                CamRec.setOutputFormat(MediaRecorder.OutputFormat.WEBM);
                CamRec.setVideoEncoder(MediaRecorder.VideoEncoder.VP8);
                CamRec.setVideoFrameRate(30);
                CamRec.setVideoSize(1280, 720);
                CamRec.setVideoEncodingBitRate(1500*1000);
                CamRec.setPreviewDisplay(surfaceHolder.getSurface());

                try{
                    LocalServerSocket server = new LocalServerSocket("LocalServer");

                    receiver = new LocalSocket();
                    receiver.connect(server.getLocalSocketAddress());
                    receiver.setReceiveBufferSize(1500*1000);

                    sender = server.accept();
                    sender.setSendBufferSize(1500*1000);

                    CamRec.setOutputFile(sender.getFileDescriptor());

                    CamRec.prepare();

                    CamRec.start();
                }
                catch (Exception ex){
                    Log.d("VoIP", "VideoHandler prepare/start: " + ex+"\n"+ex.getMessage());
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });
    }

    public void StartLocalCam(){
    }

    public void StartReceiveRemoteCam(final SurfaceView sView, final int remotePort, final Context context) {
        sView.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                try {
                    MediaPlayer player = new MediaPlayer();

                    player.setDisplay(sView.getHolder());

                    player.setDataSource(context, Uri.parse("udp://193.93.131.10:" + remotePort));

                    player.prepare();

                    player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            Log.d("SimpleVideoPlayer", "Starting player");
                            mp.start();
                        }
                    });

                    player.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.d("SimpleVideoPlayer", "error with code: " + what);
                            return false;
                        }
                    });
                } catch (Exception ex) {
                    Log.d("VoIP", "StartRemoteCam: " + ex + "\n" + ex.getMessage());
                }
            }


            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });
    }

            public void Start(){

                try {
                    CamRec.prepare();
                    CamRec.start();
                }
                catch (Exception ex){
                    Log.d("VoIP", "VideoHandler prepare/start: " + ex+"\n"+ex.getMessage());
                }

                final byte[] mdat = { 'm', 'd', 'a', 't' };

                new Thread() {

                    public void run() {
                        byte[] buffer = new byte[mdat.length];
                        int BufferSize = 0;

                        while(true){
                            try {
                        /*if (!Arrays.equals(buffer, mdat)) {*/
                                BufferSize = sender.getReceiveBufferSize();
                                Log.d("VoIP", "BuffSize: " + BufferSize);
                                //}


                                buffer = new byte[mdat.length];
                                Thread.sleep(1);
                            }
                            catch (Exception ex){
                                Log.d("VoIP", "VideoHandler start: " + ex.getMessage());
                            }
                        }
                    }
                }.start();
            }

        }