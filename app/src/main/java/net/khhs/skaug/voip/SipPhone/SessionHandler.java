package net.khhs.skaug.voip.SipPhone;

import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.rtp.RtpStream;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class SessionHandler {

    public String RemoteProxyAddress;
    public String RemoteAddress;
    public String LocalAddress;
    public String RemoteNumber;
    public int RemoteAudioPort;
    public int RemoteVideoPort;
    public int LocalVideoPort = 6058;
    Thread SessionHandlerThread;

    AudioStream SessionAudioStream;
    AudioGroup audioGroup;

    public SessionHandler(String sessionDescription){

        final String[] SdpArr = sessionDescription.split("\r\n");

        SessionHandlerThread = new Thread() {

            public void run() {

                for (int i = 0; i < SdpArr.length; i++) {
                    if (SdpArr[i].startsWith("o=")) {
                        String[] RemoteConfigArr = SdpArr[i].split(" ");
                        try {
                            RemoteNumber = RemoteConfigArr[0].substring(RemoteConfigArr[0].indexOf("+"));
                            RemoteAddress = RemoteConfigArr[RemoteConfigArr.length - 1];
                        } catch (Exception ex) {
                            Log.d("VoIP", ex.getMessage());
                        }
                    } else if (SdpArr[i].startsWith("c=")) {
                        String[] ProxyConfigArr = SdpArr[i].split(" ");
                        try {
                            RemoteProxyAddress = ProxyConfigArr[ProxyConfigArr.length - 1];
                        } catch (Exception ex) {
                            Log.d("VoIP", ex.getMessage());
                        }
                    } else if (SdpArr[i].startsWith("m=audio")) {

                        String[] AduioConfigArr = SdpArr[i].split(" ");
                        RemoteAudioPort = Integer.parseInt(AduioConfigArr[1]);

                        for (int k = i + 1; k < SdpArr.length; k++) {
                            if (SdpArr[k].startsWith("a")) {

                            } else if (SdpArr[k].startsWith("m")) {
                                i = k - 1;
                                break;
                            }
                        }

                    } else if (SdpArr[i].startsWith("m=video")) {
                        String[] VideoConfigArr = SdpArr[i].split(" ");
                        RemoteVideoPort = Integer.parseInt(VideoConfigArr[1]);
                    }

                }

                try {

                    LocalAddress = GetLocalIpAddress();
                    Log.d("VoIP", "SDP info:");
                    Log.d("VoIP", RemoteProxyAddress);
                    Log.d("VoIP", RemoteAddress);
                    Log.d("VoIP", RemoteNumber);
                    Log.d("VoIP", RemoteAudioPort+"");
                    Log.d("VoIP", RemoteVideoPort+"");
                    Log.d("VoIP", LocalAddress);
                }
                catch(Exception ex){
                    Log.d("VoIP", ex+"");
                }
            }
        };
        SessionHandlerThread.start();

    }

    private String GetLocalIpAddress(){
        try {
        for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
            NetworkInterface intf = en.nextElement();
            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                InetAddress inetAddress = enumIpAddr.nextElement();
                if (!inetAddress.isLoopbackAddress()) {
                    if(!inetAddress.getHostAddress().contains(":")){
                        return inetAddress.getHostAddress();
                    }
                }
            }
        }
        } catch (Exception ex) {
            Log.e("VoIP", ex.toString());
        }
        return null;
    }


    public void SetupAudioStream(){
        while(SessionHandlerThread.isAlive()){

        }
        try {
            audioGroup = new AudioGroup();
            audioGroup.setMode(AudioGroup.MODE_NORMAL);

            SessionAudioStream = new AudioStream(InetAddress.getByName(LocalAddress));
            SessionAudioStream.setCodec(AudioCodec.PCMU);
            SessionAudioStream.setMode(RtpStream.MODE_NORMAL);


            Log.d("VoIP", "Audio started");
        }
        catch (Exception ex){
            Log.d("VoIP", "SetupAudio: " + ex.getMessage());
        }
    }

    public void StartAudio(){
        try {
            SessionAudioStream.associate(InetAddress.getByName(RemoteProxyAddress), RemoteAudioPort);
            SessionAudioStream.join(audioGroup);
        }
        catch (Exception ex){
            Log.d("VoIP", "Start audio: " + ex.getMessage());
        }
    }

    private String GenerateAudioCodecSessionDescription() {
        AudioCodec codec = SessionAudioStream.getCodec();
        return "m=audio "+String.valueOf(SessionAudioStream.getLocalPort())+" RTP/AVP "+codec.type+"\r\n" +
                "a=rtpmap:"+codec.type+" "+codec.rtpmap+"\r\n" +
                "m=video 25555 RTP/AVP 96\r\n" +
                "a=rtpmap:96 VP8/90000\r\n";
    }

    public String getSessionDescription() {
        StringBuilder sessionDescription = new StringBuilder();
        sessionDescription.append("v=0\r\n");
        // The RFC 4566 (5.2) suggest to use an NTP timestamp here but we will simply use a UNIX timestamp
        sessionDescription.append("o=+47580012000497 "+System.currentTimeMillis()+" "+System.currentTimeMillis()+" IN IP4 "+LocalAddress+"\r\n");
        sessionDescription.append("s=Talk\r\n");
        sessionDescription.append("c=IN IP4 "+RemoteProxyAddress+"\r\n");
        sessionDescription.append("t=0 0\r\n");
        sessionDescription.append("a=rtcp-xr:rcvr-rtt=all:10000 stat-summary=loss,dup,jitt,TTL voip-metrics\r\n");
        // Prevents two different sessions from using the same peripheral at the same time
        sessionDescription.append(GenerateAudioCodecSessionDescription());
        /*for (int i=0;i<mStreamList.length;i++) {
            if (mStreamList[i] != null) {
                sessionDescription.append(mStreamList[i].generateSessionDescription());
                sessionDescription.append("a=control:trackID="+i+"\r\n");
            }
        }*/
        return sessionDescription.toString();
    }
}
