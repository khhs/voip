package net.khhs.skaug.voip.SipPhone;

import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.rtp.RtpStream;

import java.net.InetAddress;
import java.net.SocketException;

public class AudioHandler extends AudioStream {
    private AudioGroup audioGroup;

    public AudioHandler(InetAddress address) throws SocketException {
        super(address);


        audioGroup = new AudioGroup();
        audioGroup.setMode(AudioGroup.MODE_ECHO_SUPPRESSION);

        this.setCodec(AudioCodec.PCMU);
        this.setCodec(AudioCodec.PCMA);
        this.setMode(RtpStream.MODE_NORMAL);
    }


}
