package net.khhs.skaug.voip;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.sip.SipAudioCall;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import net.khhs.skaug.voip.SipPhone.PhoneManager;

public class DialActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    EditText TxtNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dial);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart(){
        super.onStart();

        TxtNumber = ((EditText) findViewById(R.id.etTxtNumber));

        PhoneManager.InitPhone(this);
        //PhoneManager.
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void BtnCall_OnClick(View view) {
        PhoneManager.Call(TxtNumber.getText()+"", new SipAudioCall.Listener(){

            @Override
            public void onCallBusy(SipAudioCall call){
                Log.d("VoIP", "onCallBusy");
            }

            @Override
            public void onCallEnded(SipAudioCall call){
                Log.d("VoIP", "onCallEnded");
                call.setSpeakerMode(false);
            }

            @Override
            public void	onCallEstablished(SipAudioCall call){
                Log.d("VoIP", "onCallEstablished");
                call.startAudio();
                call.setSpeakerMode(true);
            }

            @Override
            public void	onCallHeld(SipAudioCall call){
                Log.d("VoIP", "onCallHeld");
            }

            @Override
            public void	onCalling(SipAudioCall call){
                Log.d("VoIP", "onCalling");
            }

            @Override
            public void	onChanged(SipAudioCall call){
                Log.d("VoIP", "onChanged");
            }

            @Override
            public void	onError(SipAudioCall call, int errorCode, String errorMessage){
                Log.d("VoIP", "onError:" + errorMessage);
            }

            @Override
            public void	onReadyToCall(SipAudioCall call){
                Log.d("VoIP", "onReadyToCall");
            }

            @Override
            public void	onRinging(SipAudioCall call, SipProfile caller){
                Log.d("VoIP", "onRinging: " + caller.getUserName());
            }

            @Override
            public void	onRingingBack(SipAudioCall call){
                Log.d("VoIP", "onRingingBack");
            }

        });

    }

    public void BtnDialPad_OnClick(View view) {
        if(view.getId() == R.id.BtnDialZero)
            TxtNumber.append("0");
        else if(view.getId() == R.id.BtnDialOne)
            TxtNumber.append("1");
        else if(view.getId() == R.id.BtnDialTwo)
            TxtNumber.append("2");
        else if(view.getId() == R.id.BtnDialThree)
            TxtNumber.append("3");
        else if(view.getId() == R.id.BtnDialFour)
            TxtNumber.append("4");
        else if(view.getId() == R.id.BtnDialFive)
            TxtNumber.append("5");
        else if(view.getId() == R.id.BtnDialSix)
            TxtNumber.append("6");
        else if(view.getId() == R.id.BtnDialSeven)
            TxtNumber.append("7");
        else if(view.getId() == R.id.BtnDialEight)
            TxtNumber.append("8");
        else if(view.getId() == R.id.BtnDialNine)
            TxtNumber.append("9");
        else if(view.getId() == R.id.BtnDialStar)
            TxtNumber.append("*");
        else if(view.getId() == R.id.BtnDialHashtag)
            TxtNumber.append("#");
        else if(view.getId() == R.id.BtnDialPlus)
            TxtNumber.append("+");
        else if(view.getId() == R.id.BtnDialDelete) {
            try {
                TxtNumber.getText().delete(TxtNumber.getText().length() - 1, TxtNumber.getText().length());
            }
            catch(Exception ex) {}
        }
    }
}
