package net.khhs.skaug.voip;

import android.net.sip.SipProfile;
import android.net.sip.SipSession;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;

import net.khhs.skaug.voip.SipPhone.SessionHandler;
import net.khhs.skaug.voip.SipPhone.PhoneManager;
import net.khhs.skaug.voip.SipPhone.VideoHandler;

import static android.net.sip.SipManager.getCallId;
import static android.net.sip.SipManager.getOfferSessionDescription;

public class CallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
    }


    @Override
    protected void onStart() {
        super.onStart();

        String str = getCallId(this.getIntent());
        String str2 = getOfferSessionDescription(this.getIntent());

        final SessionHandler AudHandel = new SessionHandler(str2);
        final VideoHandler VidHandel = new VideoHandler(((SurfaceView)findViewById(R.id.svLocalCam)));

        try {
            SipSession CallSession = PhoneManager.AppSipManager.getSessionFor(this.getIntent());
            AudHandel.SetupAudioStream();

            String stras = AudHandel.getSessionDescription();
            Log.d("VoIP", stras);

            CallSession.answerCall(stras, 50);
            CallSession.setListener(new SipSession.Listener(){

                @Override
                public void onCallBusy(SipSession session){
                    Log.d("VoIP", "onCallBusy");
                }

                @Override
                public void onCallEnded(SipSession session){
                    Log.d("VoIP", "onCallEnded");
                }

                @Override
                public void	onCallEstablished(SipSession session, String sessionDescription){
                    Log.d("VoIP", "onCallEstablished");
                    AudHandel.StartAudio();
                    VidHandel.StartLocalCam();
                    VidHandel.StartReceiveRemoteCam(((SurfaceView)findViewById(R.id.svRemoteCam)), AudHandel.RemoteVideoPort, getApplicationContext());
                }

                @Override
                public void	onCallChangeFailed(SipSession session, int errorCode, String errorMessage){
                    Log.d("VoIP", "onCallHeld");
                }

                @Override
                public void	onCalling(SipSession session) {
                    Log.d("VoIP", "onCalling");
                }

                @Override
                public void	onError(SipSession session, int errorCode, String errorMessage){
                    Log.d("VoIP", "onError:" + errorMessage);
                }

                @Override
                public void	onRinging(SipSession session, SipProfile caller, String sessionDescription){
                    Log.d("VoIP", "onRinging: " + caller.getUserName());
                }

                @Override
                public void	onRingingBack(SipSession session) {
                    Log.d("VoIP", "onRingingBack");
                }
            });
        }
        catch (Exception ex){
            Log.d("VoIP", ex.getMessage());
        }
    }


}
