package net.khhs.skaug.voip.SipPhone;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.sip.SipAudioCall;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.net.sip.SipSession;
import android.util.Log;

import net.khhs.skaug.voip.CallActivity;

public class PhoneManager {

    public static SipManager AppSipManager;
    private static SipProfile RegisteredProfile;
    private static Boolean IsPhoneInitiated = false;
    private static Intent IncomingCallIntent;
    private static Context ApplicationContext;

    public static void InitPhone(Context context){
        if(IsPhoneInitiated == false) {
            ApplicationContext = context;
            AppSipManager = SipManager.newInstance(context);
            IsPhoneInitiated = true;

            Register("+47580012000497", "demosip.ipco.no", "Vel2016Ferd!", PendingIntent.getActivity(context, 5829,
                    new Intent(context, CallActivity.class), PendingIntent.FLAG_CANCEL_CURRENT), new SipRegistrationListener() {
                @Override
                public void onRegistering(String s) {
                    Log.d("VoIP", "onRegistering");
                }

                @Override
                public void onRegistrationDone(String s, long l) {
                    Log.d("VoIP", "onRegistrationDone");
                }

                @Override
                public void onRegistrationFailed(String s, int i, String s1) {
                    Log.d("VoIP", "onRegistrationFailed: " + s +" : "+i+"\n" + s1);
                }
            });
        }

    }

    private static boolean CheckPhoneState(String checkState){
        try {
            if (checkState.equals("init") && !IsPhoneInitiated) {
                Log.d("VoIP", "Phone must be initiated first");
                return false;
            }
            else if (checkState.equals("registered") && !AppSipManager.isRegistered(RegisteredProfile.getUriString())) {
                Log.d("VoIP", "Phone must be registered first: " + AppSipManager.isRegistered(RegisteredProfile.getUriString()));
                return false;
            }
        }
        catch(Exception ex){
            Log.d("VoIP", "Check: " + ex.getMessage());
        }
        return true;
    }

    public static void Register(String username, String domain, String password, PendingIntent callIntent, SipRegistrationListener listener){
        if(!CheckPhoneState("init"))
            return;
        SipProfile.Builder builder = null;
        try {
            builder = new SipProfile.Builder(username, domain);
        }
        catch (Exception ex){

        }
            builder.setPassword(password);
            builder.setAutoRegistration(false);
            builder.setSendKeepAlive(false);
            builder.setDisplayName("Karl");
            builder.setAuthUserName("+47580012000497");
            RegisteredProfile = builder.build();

        try {
            AppSipManager.open(RegisteredProfile);
            AppSipManager.register(RegisteredProfile, 90, listener);
        }
        catch(Exception ex) {
            Log.d("VoIP", "Register 1: " + ex.getMessage());
        }
    }

    private static void TryReg(SipProfile locProf, PendingIntent callInt, SipRegistrationListener list){
        try {
            //AppSipManager.unregister(RegisteredProfile, null);
            //AppSipManager.register(RegisteredProfile, 900, list);
            //AppSipManager.close(RegisteredProfile.getUriString());
        }
        catch(Exception ex) {
            Log.d("VoIP", "Register 3: " + ex.getMessage());
        }
    }


    public static void Call(String number, SipAudioCall.Listener listener){
        if(!CheckPhoneState("registered"))
            return;
        try {
            AppSipManager.makeAudioCall(RegisteredProfile.getUriString(), "sip:" + number + "@" + RegisteredProfile.getSipDomain(), listener, 45);
        }
        catch (Exception ex){
            Log.d("VoIP", ex+"");
        }
    }

}
